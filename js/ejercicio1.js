//Codigo variables globales



//Eventos o funciones

//Ejercicio 1 y 1b (cambiar color click en circulos )

$(document).on("click", "div.circulo", function () {
    $(this).addClass("colorrojo");
})

$(document).on("click", "#rojo", function () {
    $("div.circulo").addClass("colorrojo");
})


$(document).on("click", "#quitar", function () {
    $("div.circulo").removeClass("colorrojo");
})

//Ejercicio 2 (poner texto de input en otro input)

$(document).on("click", "#ponabajo", function () {
    var x = $("#text1").val();
    $("#text2").val(x);
})

//Ejercicio 2b (cambiar celdas de tabla y concatenar con append)

$(document).on("click", "#pontabla", function () {
    var x = $("#textotabla").val();

    $("p").text(x);
})
$(document).on("click", "#ponta", function () {
    var x = $("#textotabla").val();

    $("p").append(x);
})

var x = $("#textonumero").val();
var num = parseInt(x);
if (num == 0) {
    $("#bajar").attr('disabled', 'disabled');
}


//Ejercicio 4 (bajar y subir una unidad con boton)

$(document).on("click", "#bajar", function () {
    var x = $("#textonumero").val();
    var num = parseInt(x);
    if (num == 10)
        $('#subir').removeAttr('disabled');
    if (num == 1) {
        $("#textonumero").val(num - 1);
        $("#bajar").attr('disabled', 'disabled');

    } else {
        $("#textonumero").val(num - 1);
    }

})

$(document).on("click", "#subir", function () {
    var x = $("#textonumero").val();
    var num = parseInt(x);
    if (num == 0)
        $('#bajar').removeAttr('disabled');
    if (num == 9) {
        $("#textonumero").val(num + 1);
        $("#subir").attr('disabled', 'disabled');

    } else {
        $("#textonumero").val(num + 1);
    }

})

//Ejercicio 5 (sorteo aloetorio con random)

var interval = null; //Variable utilizada en setInterval para secuenciar random

$(document).on("click", "#calcularandom", function () {


    clearInterval(interval);
    var valor = Math.floor(Math.random() * 50);
    $("span.sorteo1").text(valor);
    var valor = Math.floor(Math.random() * 50);
    $("span.sorteo2").text(valor);
    var valor = Math.floor(Math.random() * 50);
    $("span.sorteo3").text(valor);


})


$(document).on("click", "#sorteorandom", function () {

    $("span.sorteo1").text("00");

    $("span.sorteo2").text("00");

    $("span.sorteo3").text("00");
    
        interval = setInterval(function () {
            var valor = Math.floor(Math.random() * 50);
            $("span.sorteo1").text(valor);
            var valor = Math.floor(Math.random() * 50);
            $("span.sorteo2").text(valor);
            var valor = Math.floor(Math.random() * 50);
            $("span.sorteo3").text(valor);

        }, 100);
    

})

//Ejercicio 6 (paginación con bootstrap)
//Debajo solucion con this

$(document).on("click", "#enlace0", function () {
    var x = $("#enlace1").text();
    var num = parseInt(x);
    if(num==0){
        console.log("minimo");
    }else if(num != 0){
    for(i=1 ;i<6 ;i ++){
        x = $("#enlace"+i).text();
        num = parseInt(x);
        
        $("#enlace"+i).text(num-1);
        
    }
    }
})

$(document).on("click", "#enlace6", function () {


    var x = $("#enlace5").text();
    var num = parseInt(x);

    if(num==20){
        console.log("maximo");
    }else if(num != 20){
        for(i=1 ;i<6 ;i ++){
            x = $("#enlace"+i).text();
            num = parseInt(x);
            
            $("#enlace"+i).text(num+1);
            
        }
        }


})

$(document).on("click", "#enlace1", function () {


    var x = $("#enlace1").text();
    var num = parseInt(x);

    $("#enlace").attr("href","#"+num);
    alert("Pagina" +num);


})

$(document).on("click", "#enlace2", function () {


    var x = $("#enlace2").text();
    var num = parseInt(x);
    $("#enlace"+num).attr("href","#"+num);

    alert("Pagina" +num);

})

$(document).on("click", "#enlace3", function () {

    
    var x = $("#enlace3").text();
    var num = parseInt(x);
    $("#enlace"+num).attr("href","#"+num);
    alert("Pagina" +num);

})

$(document).on("click", "#enlace4", function () {

    
    var x = $("#enlace4").text();
    var num = parseInt(x);
    $("#enlace"+num).attr("href","#"+num);
    alert("Pagina" +num);

})

$(document).on("click", "#enlace5", function () {

    
    var x = $("#enlace5").text();
    var num = parseInt(x);
    $("#enlace"+num).attr("href","#"+num);
    alert("Pagina" +num);
})

//Ejercicio 6

    
$(document).on("click", ".enlace", function () {

    
    
    $(".enlace").each(function () {
        var valor = parseInt($(this).text());
       
        $(this).text(valor);
        $(this).attr("href", "pagina.html?p="+valor);
    })
})


